import axios from 'axios';

const API_ROOT = 'https://9hu0ztgsi6.execute-api.us-east-2.amazonaws.com';

const requests = {

    get: async (url, tokenForAPI) => {
        try {
            const res = await axios.get(API_ROOT + url, tokenForAPI);
            return res;
        } catch (err) {
            return err;
        }
    },

    post: async (url, body, tokenForAPI) => {
        try {
            const res = await axios.post(API_ROOT + url, body, tokenForAPI);
            return res;
        } catch (err) {
            return err;
        }
    }
}

const General = {

    getData: () => {
        return requests.get('/test/student/grade/');
    },
}

export default {
    General,
}