import React, { Component } from 'react';
import { SemipolarLoading } from 'react-loadingg';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Home from '../Home/Home';
import './App.css';

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loader: false,
        }
    }

    toggleLoader = value => {
        this.setState({
            loader: value,
        })
    }

    render() {
        return (
            <div className="App full-space">
                <ToastContainer />
                <Home toggleLoader={this.toggleLoader} />
                <div className={this.state.loader ? 'loader-main full-space' : 'd-none'}>
                    <SemipolarLoading size='large' color='var(--primary-color)' />
                </div>
            </div>
        );
    }
}

export default (App);
