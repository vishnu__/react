import React, { Component } from 'react';
import { toast } from 'react-toastify';
import Icon from 'react-icons-kit';
import { ic_search } from 'react-icons-kit/md/ic_search';
import { ic_arrow_upward } from 'react-icons-kit/md/ic_arrow_upward';
import { ic_arrow_downward } from 'react-icons-kit/md/ic_arrow_downward';
import { ic_stop } from 'react-icons-kit/md/ic_stop';

import './Home.css';
import agent from '../../services/agent';

class Home extends Component {

    constructor(props) {
        super(props);
        this.input = React.createRef();

        this.state = {
            data: [],
            subjects: [],
            dataToRender: [],
            search: '',
            showInput: false,
            sort: {
                column: '',
                asc: false,
            },
        }
    }

    componentDidMount() {
        this.props.toggleLoader(true);
        agent.General.getData()
            .then(res => {
                this.props.toggleLoader(false);
                if (res.status === 200) {
                    let assignments = res.data.clsassignments;
                    let grade = res.data.gradedata;
                    let assignObject = {}, data = [];
                    assignments.forEach(assignment => {
                        assignObject[assignment.sectionid] = { ...assignment };
                    })
                    let subjects = assignments.map(a => a.sectionname).reduce((a, b) => (a[b] = 0, a), {});
                    grade.forEach((student) => {
                        student.grades.forEach(item => {
                            subjects[assignObject[item.sectionid].sectionname] = parseInt(item.gradePer.slice(0, -1));
                        });
                        let object = {
                            name: student.studentName,
                            id: student.studentid,
                            ...subjects
                        }
                        data.push(object);
                    });
                    this.setState({
                        data,
                        subjects: Object.keys(subjects),
                        dataToRender: data,
                    });
                } else {
                    toast.dark('Some error occured while fetching data...', {
                        pauseOnFocusLoss: false,
                        hideProgressBar: true,
                        position: toast.POSITION.TOP_CENTER,
                    });
                }
            });
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.state.sort !== prevState.sort) {
            this.sortData();
        }
        if (this.state.search !== prevState.search) {
            let list = this.state.data.filter(item => (item.name.toLowerCase().indexOf(this.state.search.toLowerCase()) > -1));
            this.setState({
                dataToRender: list,
            });
        }
    }

    focusSearch = () => {
        this.setState({
            showInput: !this.state.showInput,
        }, () => {
            if (this.state.showInput) this.input.current.focus();
        });
    }

    inputSearch = event => {
        this.setState({
            search: event.target.value,
        });
    }

    enter = event => {
        if (event.key === 'Enter') {
            this.setState({
                showInput: !this.state.showInput,
            });
        }
    }

    sort = column => {
        let sort = { ...this.state.sort };
        if (sort.column === column) {
            sort = {
                ...sort,
                asc: !sort.asc,
            }
        } else {
            sort = {
                column: column,
                asc: false,
            }
        }
        this.setState({
            sort,
        });
    }

    sortData = () => {
        const { sort } = this.state;
        let data = this.state.dataToRender.map(u => Object.assign(u, {}));
        if (sort.asc) {
            data.sort((a, b) =>
                (a[sort.column] < b[sort.column])
                    ? 1
                    : ((b[sort.column] < a[sort.column])
                        ? -1
                        : 0
                    )
            )
        } else {
            data.sort((a, b) =>
                (a[sort.column] > b[sort.column])
                    ? 1
                    : ((b[sort.column] > a[sort.column])
                        ? -1
                        : 0
                    )
            )
        }
        this.setState({
            dataToRender: data
        });
    }

    render() {
        const { subjects, dataToRender, search, showInput, data, sort } = this.state;
        let width = 80 / subjects.length;
        return (
            <div className="full-space home-container">
                {!data.length
                    ? <div className='full-space home-container'>
                        There's no data to be displayed
                    </div>
                    : <div className='students-table-container'>
                        <div className='table-header-area'>
                            {showInput ? ''
                                : <Icon
                                    icon={ic_search}
                                    onClick={this.focusSearch}
                                    className={`search-icon ${search.length ? 'active' : ''}`}
                                    size={18}
                                />
                            }
                            <div
                                className='header-content-student'
                            >
                                {showInput
                                    ? <input
                                        className='search-input'
                                        onChange={this.inputSearch}
                                        onKeyDown={this.enter}
                                        onBlur={this.focusSearch}
                                        placeholder='Enter text'
                                        value={this.state.search}
                                        ref={this.input}
                                    />
                                    : <text onClick={() => this.sort('name')}>
                                        {sort.column === 'name'
                                            ? (sort.asc
                                                ? <Icon icon={ic_arrow_downward} />
                                                : <Icon icon={ic_arrow_upward} />
                                            )
                                            : <Icon icon={ic_stop} />
                                        }
                                        Student Name
                                    </text>
                                }
                            </div>
                            {subjects.map((subject, index) => {
                                return <div
                                    key={index}
                                    className='header-content-student'
                                    onClick={() => this.sort(subject)}
                                    style={{ width: `${width}%` }}
                                >
                                    {sort.column === subject
                                        ? (sort.asc
                                            ? <Icon icon={ic_arrow_downward} />
                                            : <Icon icon={ic_arrow_upward} />
                                        )
                                        : <Icon icon={ic_stop} />
                                    }
                                    {subject}
                                </div>
                            })}
                        </div>
                        <div className='table-content-area'>
                            {!dataToRender.length
                                ? <div className={`row-content information`}>
                                    No search results
                                </div>
                                : dataToRender.map((row, index) => {
                                    return <div key={index} className={`row-content${index % 2 ? ' dark' : ''}`}>
                                        <div className='row-cell'>{row.name}</div>
                                        {subjects.map((subject, index) => {
                                            return <div
                                                key={index}
                                                className='row-cell'
                                                style={{ width: `${width}%` }}
                                            >
                                                {row[subject]}
                                            </div>
                                        })}
                                    </div>
                                })
                            }
                        </div>
                    </div>
                }
            </div>
        )
    }
}

export default (Home);